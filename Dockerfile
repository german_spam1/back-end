# Imagen docker base
FROM node

#Definimos directorio de trabajo
WORKDIR /docker-api

#Copiar archivos del proyecto
ADD . /docker-api

# RUN npm install

#Puerto donde exponemos contenedor
EXPOSE 3000

#Comando para lanzar la app
CMD ["npm",  "start"]
