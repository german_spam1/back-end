
module.exports = {
  Collection: 'user?',
  GetUserById: function (id) {
    return 'q={"id":' + id + '}&f={"_id":0,"first_name":1,"last_name":1,"email":1,"password":1}&';
  },
  GetUserByEmail: function (email) {
    return 'q={"email":"' + email + '"}&';
  },
  ExcudeInternalId: function () {
    return 'f={"_id":0}&';
  },
  GetMaxId: function () {
    return 's={"id": -1}&l=1&';
  },
  GetUserAccounts: function () {
    return 'f={"_id":0, "account":1}&';
  },
  GetUserAccount: function (idUser, idAccount) {
    return 'q={"id":' + idUser + ', account:{ "id":' + idAccount + '}}&';
  },
  UpdatePassword: function(password)
  {
    return '{"$set":{"password":"' + password + '"}}';
  },
  Login: function(email, password)
  {
    return 'q={"email": "' + email + '", "password":"' + password + '"}&';
  },
  SetLoggedIN: function()
  {
    return '{"$set":{"logged":true}}';
  },
  SetLoggedOUT: function()
  {
    return '{"$unset":{"logged":""}}';
  }
};
