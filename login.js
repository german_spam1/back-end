// # Imports
var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json')
let config = require('./Database/config.js');
let querys = require('./Database/querys.js');
let msg = require('./Configuration/message.js');
let code = require('./Configuration/statusCodes.js');
let keys = require('./Security/keys.js');
var jwt = require('jsonwebtoken');

var app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({limit:'10mb'}));
var port = process.env.PORT || 3000;

app.listen(port);
console.log('Escuchando en el puerto 3000...');
var httpClient = requestJSON.createClient(config.baseMLabURL);

// LOGIN POST
app.post(config.URI + 'login',
  function(request, response) {
    var email = request.body.email;
    var password = request.body.password;

    var query = querys.Login(email,password);

    httpClient.get(querys.Collection + query + keys.apikeyMLab,
      function(error, resp, body) {
         if (!error) {
           if (body.length == 1)
           {
             var tokenData = {
               username: email
               // ANY DATA
             };

             var token = jwt.sign(tokenData, keys.jwtKey, {
                expiresIn: 60 * 5 // expires in 5 minutos
             });

             httpClient = requestJSON.createClient(config.baseMLabURL);
             var setLoggedIN = querys.SetLoggedIN();
             httpClient.put(querys.Collection + query + keys.apikeyMLab, JSON.parse(setLoggedIN),
               function(errorLogged, resLogged, bodyLogged) {
                 response.status(code.OK).send({ token, "login":"ok", "id":body[0].id});
             });
           }
           else {
             response.status(code.NotFound).send(msg.InvalidUsernameOrPass);
           }
         }
         else {
           console.log("Error: " + error);
           response.status(code.InternalServerError).send(msg.Error);
         }
       });
    });

// LOGOUT POST
app.post(config.URI + 'logout',
  function(request, response) {
    console.log("POST Logout");
    var email = request.body.email;

    var query = querys.GetUserByEmail(email);

    httpClient.get(querys.Collection + query +  keys.apikeyMLab,
      function(error, resp, body) {
       if (!error) {

         if (body.length == 1)
         {
             console.log("Sesion cerrada correctamente");
             httpClient = requestJSON.createClient(config.baseMLabURL);
             var setLoggedOUT = querys.SetLoggedOUT();
             httpClient.put(querys.Collection + query + keys.apikeyMLab, JSON.parse(setLoggedOUT),
               function(errorLogged, resLogged, bodyLogged) {
                 response.send({"logout":"ok", "id":body[0].id})
             })
           }
           else {
             response.status(code.NotFound).send(msg.InvalidEmail)
           }
         }
      else {
        console.log("Error: " + error);
        response.status(code.InternalServerError).send(msg.Error)
      }
    });
});
