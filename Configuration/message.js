//Commons
const Error = {'error':'An error has occurred'};
const NotFound = {'error':'An error has occurred'};

// User
const PasswordChanged = {'Message':'Password changed'};
const UserNotFound = {'Message':'User not found'};
const UserAllreadyExists = {'Message':'User allready exists'};

// Accounts
const AccountsNotFound = {'Message':'Accounts not found'};

//Login / Logout
const InvalidEmailOrPass = {'Message':'Invalid email or password'};
const InvalidEmail = {'Message':'Invalid email'};

module.exports ={
  Error: Error,
  UserNotFound: UserNotFound,
  UserAllreadyExists: UserAllreadyExists,
  AccountsNotFound: AccountsNotFound,
  PasswordChanged: PasswordChanged,
  InvalidEmailOrPass: InvalidEmailOrPass,
  InvalidEmail: InvalidEmail
};
