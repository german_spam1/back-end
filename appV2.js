var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json')
var app = express();
var port = process.env.PORT || 3000;
const URI = "/api-uruguay/v1/";
app.use(bodyParser.json());

app.listen(port);
console.log('Escuchando en el puerto 3000...');

var apikeyMLab = "apiKey=Wv4iLVms1eGGIlHgzCvn41NIN9zg3Mrc";
var baseMLabURL = "https://api.mlab.com/api/1/databases/techubduruguay/collections/";

// GET users consumiendo API REST de mLab
app.get(URI + 'users',
  function(req, res) {
    console.log("GET ");
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// Petición GET con id en mLab
app.get(URI + 'users/:id',
  function (req, res) {
    console.log("GET /colapi/v3/users/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});


// POST 'users' mLab
app.post(URI + 'users',
  function(req, res){
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + apikeyMLab,
      function(error, respuestaMLab, body){
        newID = body.length + 1;
        console.log("newID:" + newID);
        var newUser = {
          "id" : newID + 1,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };
        clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
          function(error, respuestaMLab, body){
            res.send(body);
          });
      });
  });
