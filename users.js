// # Imports
var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json')
let config = require('./Database/config.js');
let querys = require('./Database/querys.js');
let msg = require('./Configuration/message.js');
let code = require('./Configuration/statusCodes.js');
let keys = require('./Security/keys.js');

// # Initial configuration
var app = express();
var port = process.env.PORT || 3000;
app.use(bodyParser.json());
app.listen(port);
console.log('Escuchando en el puerto 3000...');
var httpClient = requestJSON.createClient(config.baseMLabURL);

// Methods
// GET users
app.get(config.URI + 'users',
  function(request, response) {

    var queryExcludeId = querys.ExcudeInternalId();
    httpClient.get(querys.Collection + queryExcludeId + keys.apikeyMLab,
      function(err, resp, body) {
        if(err) {
            console.log("Error: " + error);
            response.status(code.InternalServerError).send(msg.Error);
        } else {
          if(body.length > 0) {
            response.status(code.OK).send(body);
          } else {
            response.status(code.NotFound).send(msg.UserNotFound);
          }
        };
      });
});


// GET user by Id
app.get(config.URI + 'users/:id',
  function (request, response) {

    var id = request.params.id;
    console.log("CALL GET user by Id: " + id);
    var queryUser = querys.GetUserById(id);
    httpClient.get(querys.Collection + queryUser + keys.apikeyMLab,
      function(err, respuestaMLab, body){
        if(err) {
            console.log("Error: " + error);
            response.status(code.InternalServerError).send(msg.Error);
        } else {
          if(body.length > 0) {
            response.status(code.OK).send(body);
          } else {
            response.status(code.NotFound).send(msg.UserNotFound);
          }
        }
      });
});

//funcion para control cuando usuario ya existe
var existe = function(response)
{
  response.status(code.InternalServerError).send(msg.UserAllreadyExists);
};

//funcion para control cuando usuario no existe y se debe crear<
var noExiste = function(pFistName, pLastName, pEmail, pPassword, response)
{
  var queryIdMax = querys.GetMaxId();
  httpClient.get(querys.Collection + queryIdMax + keys.apikeyMLab,
    function(err, respuestaMax, body){
      var newId = body[0].id + 1;
      var newUser = {
        "id" : newId,
        "first_name": pFistName,
        "last_name": pLastName,
        "email" : pEmail,
        "password" : pPassword
      };
      httpClient.post(config.baseMLabURL + "user?" + keys.apikeyMLab, newUser,
        function(error, respuestaMLab, body){
          if(error) {
              console.log("Error: " + error);
              response.status(code.InternalServerError).send(msg.Error);
          } else {
              response.status(code.Created).send(body);
          };
        });
    });
};

// POST users
app.post(config.URI + 'users',
  function(request, response){

    //email del nuevo usuario a crear
    var fistNameNuevo =  request.body.first_name;
    var lastNameNuevo =  request.body.last_name;
    var emailNuevo =  request.body.email;
    var passwordNuevo =  request.body.password;

    //Query para buscar el usuario por email
    var queryUsrExiste = querys.GetUserByEmail(emailNuevo);

    httpClient.get(querys.Collection + queryUsrExiste + keys.apikeyMLab,
      function(err, respuestaMLab, bodyExiste){
          if(err) {
              console.log("Error: " + error);
              response.status(code.InternalServerError).send(msg.Error);
            }
            else {
              var mLabresponse = {};
              if(bodyExiste.length > 0) {
                  mLabresponse = bodyExiste;
                  existe(response);
              }
              else {
                  noExiste(fistNameNuevo, lastNameNuevo, emailNuevo, passwordNuevo, response);
              };
            };
        });
  });

  // PUT users
  app.put(config.URI + 'users/:id',
    function(request, response){

      //email del nuevo usuario a crear
      var id = request.params.id;
      var email =  request.body.email;
      var password =  request.body.password;

      //Query para actualizar el usuario por email
      var queryGetUser = querys.GetUserById(id);
      var queryUpdatePwd = JSON.parse(querys.UpdatePassword(password));

      httpClient.put(querys.Collection + queryGetUser + keys.apikeyMLab, queryUpdatePwd,
        function(err, respuestaMLab, body){

            if(err) {
                console.log("Error: " + error);
                response.status(code.InternalServerError).send(msg.Error);
              }
              else {
                if(body.n > 0) {
                    response.status(code.OK).send(msg.PasswordChanged);
                }
                else {
                    response.status(code.NotFound).send(msg.UserNotFound);
                };
              };
          });
 });
