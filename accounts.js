var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json')
let keys = require('./Security/keys.js');
let config = require('./Database/config.js');
let querys = require('./Database/querys.js');
let code = require('./Configuration/statusCodes.js');
let msg = require('./Configuration/message.js');
var app = express();
var port = process.env.PORT || 3000;
app.use(bodyParser.json());

app.listen(port);
console.log('Escuchando en el puerto 3000...');

var httpClient = requestJSON.createClient(config.baseMLabURL);

// GET users consumiendo API REST de mLab
app.get(config.URI + 'users/:id/accounts',
  function(request, response) {
    var id = request.params.id;
    var queryUser = querys.GetUserById(id);
    var queryAccounts = querys.GetUserAccounts();
    httpClient.get(querys.Collection + queryUser + queryAccounts + keys.apikeyMLab,
      function(err, resp, body) {
        if(err) {
            response.status(code.InternalServerError).send(msg.Error);
        } else {
          if(body.length > 0) {
            response.status(code.OK).send(body);
          } else {
            response.status(code.NotFound).send(msg.AccountsNotFound);
          }
        }
      });
});
